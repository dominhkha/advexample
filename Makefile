PATH = $(module load python/3.6)
SCRIPT = $(py3 ./hello.py)
.PHONY: run

run:
	$(PATH)
	$(SCRIPT)